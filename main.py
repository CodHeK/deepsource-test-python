# -*- coding: utf-8 -*-
# import gkeepapi
import pprint
import json
from os import walk
from flask import Flask, jsonify
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app)

pp = pprint.PrettyPrinter(indent=4)

def getWords():
    notesData = []
    dir = './notes'

    for _, _, filenames in walk(dir):
        for filename in filenames:
            noteData = open(dir + '/' + filename, 'r')
            note = {}
            letter = filename.split('.')[0]

            note['title'] = letter
            note['text'] = noteData.readlines()

            notesData.append(note)

    return notesData
    

def process(text):
    lines = []
    line = ''

    for l in text:
        if l != '\n':
            line += (l + ':')
        else:
            lines.append(line)
            line = ''
    
    if line != '':
        lines.append(line)
    
    return lines


def extract(text):
    text = text.strip('\n')

    return [ prop for prop in text.split('/') ]


def getCSVWords(words):
    word_string = ""
    for word, _ in words.items():
        word_string += word + ","
    
    return word_string


@app.route('/update', methods=['GET'])
@cross_origin()
def getNotes():
    # username = 'gaganganapathyas@gmail.com'
    # password = 'gagan30101998'

    # keep = gkeepapi.Keep()
    # success = keep.login(username, password)

    # if success:
        # gnotes = keep.find(labels=[keep.findLabel('Barrons 300')])
    
    gnotes = getWords()

    notes = {}

    for note in gnotes:
        lines = process(note['text'])
        title = note['title']

        notes[title] = {}

        for line in lines:
            try:
                word, meanings, sentences = line.split(':')[0:-1]

                meanings = extract(meanings)
                sentences = extract(sentences)

                notes[title][word] = {
                    'meanings': meanings,
                    'sentences': sentences
                }
            except:
                print(line)

    # for letter, _ in notes.items():
    #     print(getCSVWords(notes[letter]))
    #     print("\n\n")
    
    return jsonify(notes)
    
if __name__ == '__main__':
    app.run(debug=True)
